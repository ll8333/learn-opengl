#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include "GL/glew.h"
#include <QOpenGLWidget>

#include <QTimer>

#include "graph/triangle.h"

class MainWidget : public QOpenGLWidget
{
    Q_OBJECT

public:
    MainWidget(QWidget *parent = nullptr);
    ~MainWidget();

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int w, int h) override;

private:
    QTimer *m_updateTimer;

    GLfloat m_rgb[3];

    Triangle m_triangle;

};
#endif // MAINWIDGET_H
