#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "basicgrapha.h"

struct glObj
{
    GLuint shaderPrograme;
    GLuint VBO;
    GLuint VAO;

};

class Triangle : public BasicGrapha
{
public:
    Triangle();

    glObj globj;
    void initShaders(GLuint &shaderPrograme) override;
    void initTexture(GLuint &VAO, GLuint &VBO, GLfloat *vertices) override;
    void draw(GLuint shaderProgram, GLfloat *vertices) override;
};

#endif // TRIANGLE_H
