#ifndef CUBE_H
#define CUBE_H

#include "basicgrapha.h"

struct glObj
{
    GLint shaderPrograme;
    GLuint VBO;
    GLuint VAO;

    GLint mvpLoc;
    GLfloat  *vertices;
    GLuint   *indices;

};

class Cube : public BasicGrapha
{
public:
    Cube();

    glObj globj;
    void initShaders(GLuint &shaderPrograme) override;
    void initTexture(GLuint &VAO, GLuint &VBO, GLfloat *vertices) override;
    void draw(GLuint shaderProgram, GLfloat *vertices) override;

};

#endif // CUBE_H
