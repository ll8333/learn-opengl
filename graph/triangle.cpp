#include "triangle.h"

Triangle::Triangle()
{

}

void Triangle::initShaders(GLuint &shaderPrograme)
{
    const char *vertexShaderSource =
    {
        "#version 300 es                          \n"
        "layout(location = 0) in vec4 vPosition;  \n"
        "void main()                              \n"
        "{                                        \n"
        "   gl_Position = vPosition;              \n"
        "}                                        \n"
    };
    const char *fragmentShaderSource =
    {
        "#version 300 es                              \n"
        "precision mediump float;                     \n"
        "out vec4 fragColor;                          \n"
        "void main()                                  \n"
        "{                                            \n"
        "   fragColor = vec4 ( 1.0, 0.0, 0.0, 1.0 );  \n"
        "}                                            \n"
    };


    // vertex shader
    int vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShader);

    // check for shader compile errors
    testCompileShader(vertexShader,"vertexShaderSource");

    // fragment shader
    int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);

    // check for shader compile errors
    testCompileShader(fragmentShader,"fragmentShaderSource");

    // link shaders
    shaderPrograme = glCreateProgram();
    glAttachShader(shaderPrograme, vertexShader);
    glAttachShader(shaderPrograme, fragmentShader);
    glLinkProgram(shaderPrograme);

    // check for linking errors
    testLinkShaderProgram(shaderPrograme,"Triangle shader programe");

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}

void Triangle::initTexture(GLuint &VAO, GLuint &VBO, GLfloat *vertices)
{
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
}

void Triangle::draw(GLuint shaderProgram, GLfloat *vertices)
{
    glUseProgram(shaderProgram);
    glVertexAttribPointer ( 0, 3, GL_FLOAT, GL_FALSE, 0, vertices);
    glEnableVertexAttribArray(0);
    glDrawArrays(GL_TRIANGLES,0,3);
}
