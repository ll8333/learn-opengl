#ifndef BASICGRAPHA_H
#define BASICGRAPHA_H

#include "GL/glew.h"

class BasicGrapha
{
public:
    BasicGrapha();
    ~BasicGrapha();
    void testCompileShader(GLuint shader, const char *shaderName);
    void testLinkShaderProgram(GLuint shaderProgram, const char *programName);

    virtual void initShaders(GLuint &shaderPrograme) = 0;
    virtual void initTexture(GLuint &VAO, GLuint &VBO, GLfloat *vertices) = 0;
    virtual void draw(GLuint shaderProgram, GLfloat *vertices) = 0;

};

#endif // BASICGRAPHA_H
