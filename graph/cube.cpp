#include "cube.h"

Cube::Cube()
{

}

void Cube::initShaders(GLuint &shaderPrograme)
{
    const char *vertexShaderSource =
    {
        "#version 300 es                             \n"
        "uniform mat4 u_mvpMatrix;                   \n"
        "layout(location = 0) in vec4 a_position;    \n"
        "layout(location = 1) in vec4 a_color;       \n"
        "out vec4 v_color;                           \n"
        "void main()                                 \n"
        "{                                           \n"
        "   v_color = a_color;                       \n"
        "   gl_Position = u_mvpMatrix * a_position;  \n"
        "}                                           \n"
    };
    const char *fragmentShaderSource =
    {
        "#version 300 es                                \n"
        "precision mediump float;                       \n"
        "in vec4 v_color;                               \n"
        "layout(location = 0) out vec4 outColor;        \n"
        "void main()                                    \n"
        "{                                              \n"
        "  outColor = v_color;                          \n"
        "}                                              \n"
    };

    // vertex shader
    int vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShader);

    // check for shader compile errors
    testCompileShader(vertexShader,"vertexShaderSource");

    // fragment shader
    int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);

    // check for shader compile errors
    testCompileShader(fragmentShader,"fragmentShaderSource");

    // link shaders
    shaderPrograme = glCreateProgram();
    glAttachShader(shaderPrograme, vertexShader);
    glAttachShader(shaderPrograme, fragmentShader);
    glLinkProgram(shaderPrograme);

    // check for linking errors
    testLinkShaderProgram(shaderPrograme,"Cube shader programe");

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}

void Cube::initTexture(GLuint &VAO, GLuint &VBO, GLfloat *vertices)
{
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
}

void Cube::draw(GLuint shaderProgram, GLfloat *vertices)
{
    glUseProgram(shaderProgram);
    glVertexAttribPointer ( 0, 3, GL_FLOAT, GL_FALSE, 0, vertices);
    glEnableVertexAttribArray(0);
    glDrawArrays(GL_TRIANGLES,0,3);
}
