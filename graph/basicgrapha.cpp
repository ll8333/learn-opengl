#include "basicgrapha.h"
#include <iostream>

BasicGrapha::BasicGrapha()
{

}

BasicGrapha::~BasicGrapha()
{

}

void BasicGrapha::testCompileShader(GLuint shader, const char *shaderName)
{
    int success;
    char infoLog[512];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        std::cout << "ERROR:: " << shaderName << " \n" << infoLog << std::endl;
    }
    else
    {
        std::cout << "SUCCESS:: " << shaderName << " \n" << std::endl;
    }
}

void BasicGrapha::testLinkShaderProgram(GLuint shaderProgram, const char *programName)
{
    int success;
    char infoLog[512];
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cout << "ERROR:: " <<  programName << " \n" << infoLog << std::endl;
    }
    else
    {
        std::cout << "SUCCESS:: " << programName << " \n" << std::endl;
    }
}
