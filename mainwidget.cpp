#include "mainwidget.h"
#include <QtMath>
#include <iostream>

float TriangleVertices[] = {
       -0.5f, -0.5f, 0.0f, // left
        0.5f, -0.5f, 0.0f, // right
        0.0f,  0.5f, 0.0f  // top
   };

float RectVertices[] = {
       -0.5f, -0.5f, 0.0f, // left-bottom
        0.5f, -0.5f, 0.0f, // right-bottom
        0.5f,  0.5f, 0.0f, // right-top
       -0.5f,  0.5f, 0.0f, // left-top
   };

MainWidget::MainWidget(QWidget *parent)
    : QOpenGLWidget(parent)
{
    m_updateTimer = new QTimer(this);
    m_updateTimer->start(100);
    connect(m_updateTimer,SIGNAL(timeout()),this,SLOT(update()));

}

MainWidget::~MainWidget()
{

}

void MainWidget::initializeGL()
{
    glewInit();

    glClearColor(0.0,0.0,0.0,0.0);
    glClear(GL_COLOR_BUFFER_BIT);

    m_triangle.initShaders(m_triangle.globj.shaderPrograme);
    glViewport(0,0,this->width(),this->height());
}

void MainWidget::paintGL()
{
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    m_triangle.draw(m_triangle.globj.shaderPrograme,TriangleVertices);

}

void MainWidget::resizeGL(int w, int h)
{
    glViewport(0,0,w,h);
}


